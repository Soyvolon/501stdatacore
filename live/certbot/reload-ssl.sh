#!/bin/bash
docker run -i --rm --name certbot \
    -v "./docker-data/certbot/conf:/etc/letsencrypt" \
    -v "./docker-data/certbot/www/:/var/www/certbot" \
    -v "./docker-data/certbot/cred/:/cred" \
    certbot/dns-cloudflare certonly \
    --non-interactive \
    --renew-by-default \
    --agree-tos \
    --dns-cloudflare \
    --dns-cloudflare-credentials "/cred/certbot.ini" \
    -d "*.501stlegion-a3.com" \
    --email andybounds@gmail.com

# reload the nginx server, otherwise nginx will
# load config when it starts the next time.
docker exec -ti nginx nginx -s reload

